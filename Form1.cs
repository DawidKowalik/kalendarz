﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Projekt_na_zaliczenie
{
    public partial class Form1 : Form
    {

        List<Plan> myPlanList = new List<Plan>();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            String startDate = monthCalendar1.SelectionStart.ToShortDateString();
            String endDate = monthCalendar1.SelectionEnd.ToShortDateString();
            startDateLabel.Text = startDate;
            if (startDate.Equals(endDate))
            {
                endDateLabel.Text = "-";
            }
            else
            {
                endDateLabel.Text = endDate;
            }
        }
        private void monthCalendar1_DateChanged(object sender, DateRangeEventArgs e)
        {
            String startDate = monthCalendar1.SelectionStart.ToShortDateString();
            String endDate = monthCalendar1.SelectionEnd.ToShortDateString();
            startDateLabel.Text = startDate;

            if (startDate.Equals(endDate))
            {
                endDateLabel.Text = "-";
            }
            else
            {
                endDateLabel.Text = endDate;
            }

            comboBox1.Items.Clear();

            foreach (Plan plan in myPlanList)
            {
                if (startDate.Equals(plan.StartDate) && endDate.Equals(plan.EndDate))
                {
                    comboBox1.Items.Add(plan.PlanDescription);
                }
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void addPlan(object sender, EventArgs e)
        {
            MessageBox.Show("Plan został dodany", "Informacja", MessageBoxButtons.OK);

            String startDate = monthCalendar1.SelectionStart.ToShortDateString();
            String endDate = monthCalendar1.SelectionEnd.ToShortDateString();
            String planDescription = textBox1.Text;

            comboBox1.Items.Add(planDescription);

            Plan plan = new Plan(startDate, endDate, planDescription);
            myPlanList.Add(plan);

            textBox1.Text = "";

        }

        private void cancelAddingPlan(object sender, EventArgs e)
        {
            MessageBox.Show("Plan został anulowany", "Informacja", MessageBoxButtons.OK);
            textBox1.Text = ""; // textBox1 Clear
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e) { }

        private void Zakonczenie_Click(object sender, EventArgs e) { }

        private void removeChars(object sender, EventArgs e)
        {
            comboBox1.Text = "";
        }

        private void Data_Zakonczenia_Click(object sender, EventArgs e)
        {

        }
    }
}
