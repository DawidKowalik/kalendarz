﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Projekt_na_zaliczenie
{
    class Plan
    {
        private String startDate;
        private String endDate;
        private String planDescription;


        public Plan(String startDate, String endDate, String planDescription)
        {
            this.startDate = startDate;
            this.endDate = endDate;
            this.planDescription = planDescription;
        }

        public String StartDate
        {
            get => startDate;
        }

        public String EndDate
        {
            get => endDate;
        }

        public String PlanDescription
        {
            get => planDescription;
        }
    }
}